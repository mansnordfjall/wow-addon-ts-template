import { Libraries } from './constants';

const GUI = LibStub(Libraries.AceGUI);
const Console = LibStub(Libraries.AceConsole);

export function CreateWindow() {
  const frame = GUI.Create('Frame');

  frame.SetCallback('OnClose', (widget) => {
    GUI.Release(widget);
  });

  frame.SetTitle('GUI TS Example');
  frame.SetStatusText('Status bar text');
  frame.SetLayout('Flow');

  const btn = GUI.Create('Button');
  btn.SetWidth(150);
  btn.SetText('Click me!');
  btn.SetCallback('OnClick', () => {
    Console.Print('Clicked button!');
  });

  frame.AddChild(btn);
}

// Create simple example window
CreateWindow();
