declare function UnitPower(unitToken: WoWAPI.UnitId, powerType?: string, unmodified?: boolean): number;
declare function UnitPowerMax(unitToken: WoWAPI.UnitId, powerType?: string, unmodified?: boolean): number;
declare function UnitPowerType(
  unitToken: WoWAPI.UnitId,
): { powerType: number; powerToken: string; altR: number; altG: number; altB: number };

declare const PowerBarColor: any;
