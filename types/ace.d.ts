// Ace3 type defs

declare function LibStub(addonName: string): any;

declare interface LibAceAddon {
  NewAddon(addonName: string): AceAddon;
}
declare interface AceAddon {
  [key: string]: any;

  OnInitialize(): void;
  OnEnable(): void;
  OnDisable(): void;
}

declare interface AceConsole {
  [key: string]: any;

  Print(chatFrame: any, ...message: string[]): void;
  Print(message: string): void;
  Printf(chatFrame: any, format: string, ...args: any[]): void;

  RegisterChatCommand(command: string, funcName: string, persist?: boolean): void;
  UnregisterChatCommand(command: string): void;
}
