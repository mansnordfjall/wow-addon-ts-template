# WoW Addon TypeScript Template

## Run and Build
`yarn build` transpiles your TS code into Lua.  
`yarn dev` watches your files and re-transpiles whenever there is a change.

Addon Lua code is put in the `addon/Core.lua` file.
